package com.ibcs.employee.service.service.impl;

import java.util.ArrayList;
import java.util.List;
import javax.management.RuntimeErrorException;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.ibcs.employee.service.dto.EmployeeDto;
import com.ibcs.employee.service.entity.Employee;
import com.ibcs.employee.service.repository.EmployeeRepository;
import com.ibcs.employee.service.service.EmployeeService;
import com.ibcs.employee.service.util.UniqueCode;


@Service
public class EmployeeServiceImpl implements EmployeeService {

	private EmployeeRepository employeeRepository;
	private UniqueCode uniqueCode;

	public EmployeeServiceImpl(EmployeeRepository employeeRepository, UniqueCode uniqueCode) {
		this.employeeRepository = employeeRepository;
		this.uniqueCode = uniqueCode;
	}

	@Override
	public EmployeeDto create(EmployeeDto employeeDto) {
		Employee employeeEntity = new Employee();
		BeanUtils.copyProperties(employeeDto, employeeEntity);

		String code = uniqueCode.generateCode(4);

		employeeEntity.setCode(code);
		employeeEntity.setName(employeeDto.getName());
		employeeEntity.setDateOfBirth(employeeDto.getDateOfBirth());
		employeeEntity.setGender(employeeDto.getGender());
		employeeEntity.setMobile(employeeDto.getMobile());
		employeeEntity.setDepartmentId(employeeDto.getDepartmentId());

		Employee EmployeeStore = employeeRepository.save(employeeEntity);

		EmployeeDto returnValue = new EmployeeDto();
		BeanUtils.copyProperties(EmployeeStore, returnValue);

		return returnValue;

	}

	@Override
	public EmployeeDto update(Long employeeId, EmployeeDto employeeDto) {
		EmployeeDto returnValue = new EmployeeDto();

		Employee employeeEntity = employeeRepository.findById(employeeId).get();

		employeeEntity.setName(employeeDto.getName());
		employeeEntity.setDateOfBirth(employeeDto.getDateOfBirth());
		employeeEntity.setGender(employeeDto.getGender());
		employeeEntity.setMobile(employeeDto.getMobile());
		employeeEntity.setDepartmentId(employeeDto.getDepartmentId());

		Employee updateEmployee = employeeRepository.save(employeeEntity);

		BeanUtils.copyProperties(updateEmployee, returnValue);

		return returnValue;
	}

	@Override
	public void delete(Long employeeId) {
		Employee employee = employeeRepository.findById(employeeId).get();

		employeeRepository.delete(employee);
	}

	@Override
	public EmployeeDto fetchById(Long employeeId) {
		EmployeeDto returnValue = new EmployeeDto();

		Employee employeeEntity = employeeRepository.findById(employeeId).get();

		if (employeeEntity == null) {
			throw new RuntimeErrorException(null, "ID is not found");
		}
		BeanUtils.copyProperties(employeeEntity, returnValue);

		return returnValue;
	}

	@Override
	public List<EmployeeDto> fetchAll(int page, int limit) {
		List<EmployeeDto> returnValue = new ArrayList<>();

		if (page > 0)
			page = page - 1;

		Pageable pageableRequest = PageRequest.of(page, limit);
		Page<Employee> employeePage = employeeRepository.findAll(pageableRequest);
		List<Employee> employees = employeePage.getContent();

		for (Employee employee : employees) {
			EmployeeDto employeeDto = new EmployeeDto();

			BeanUtils.copyProperties(employee, employeeDto);
			returnValue.add(employeeDto);
		}

		return returnValue;
	}
	
	@Override
	public List<EmployeeDto> fetchEmployeesByDepartmentId(Long departmentId, int page, int limit) {
		List<EmployeeDto> returnValue = new ArrayList<>();

		if (page > 0)
			page = page - 1;

		Pageable pageableRequest = PageRequest.of(page, limit);
		Page<Employee> EmployeePage = employeeRepository.findByDepartmentId(departmentId, pageableRequest);
		List<Employee> Employees = EmployeePage.getContent();

		for (Employee Employee : Employees) {
			EmployeeDto EmployeeDto = new EmployeeDto();
			BeanUtils.copyProperties(Employee, EmployeeDto);
			returnValue.add(EmployeeDto);
		}

		return returnValue;
	}

}
